import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueScrollPickerComponent } from './value-scroll-picker.component';

describe('ValueScrollPickerComponent', () => {
  let component: ValueScrollPickerComponent;
  let fixture: ComponentFixture<ValueScrollPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueScrollPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueScrollPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

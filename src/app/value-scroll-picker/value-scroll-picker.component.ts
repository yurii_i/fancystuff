import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'fancy-value-scroll-picker',
    templateUrl: './value-scroll-picker.component.html',
    styleUrls: ['./value-scroll-picker.component.scss']
})
export class ValueScrollPickerComponent implements OnInit {
    public timeArray: number[];

    @Input() viewCount = 7;
    @Input() startValue = 8;
    @Input() endValue = 20;
    @Output() valueChange = new EventEmitter();

    constructor() {
    }
    ngOnInit() {
        this.timeArray = [];
        for (let i = 0, q = this.startValue; i < this.viewCount; i++) {
            this.timeArray.push(q);
            q++;

        }
    }

    public inc(): void {
        this.timeArray.shift();
        this.timeArray.push(this.timeArray[this.timeArray.length - 1] + 1);
        this.setCurrentValue();
    }
    public dec(): void {
        this.timeArray.pop();
        this.timeArray.splice(0, 0, this.timeArray[0] - 1);
        this.setCurrentValue();
    }
    private setCurrentValue() {
        this.valueChange.emit(this.timeArray[Math.floor(this.timeArray.length / 2)]);
    }

}

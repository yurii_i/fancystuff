import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavBarComponent } from '@core/nav-bar/nav-bar.component';
import { CoreModule } from '@core/core.module';
import { SpinnersComponent } from './spinners/spinners.component';
import { CoreComponent } from '@core/core.component';
import { CoreRoutingModule } from '@core/core-routing.module';
import { ValueScrollPickerComponent } from './value-scroll-picker/value-scroll-picker.component';
import { TimePickerComponent } from './time-picker/time-picker.component';
import { TimeFormatPipe } from './pipes/time-format.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SpinnersComponent,
    ValueScrollPickerComponent,
    TimePickerComponent,
    TimeFormatPipe
  ],
  exports: [
      AppComponent
  ],
  imports: [
    BrowserModule,
    CoreRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

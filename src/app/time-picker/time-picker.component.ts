import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fancy-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss']
})
export class TimePickerComponent implements OnInit {
    public timeArray: number[];
    private fromTime: Time = new Time();
    private toTime: Time = new Time();

    constructor() {
    }
    ngOnInit() {
        this.fromTime.hours = 0;
        this.fromTime.minutes = 0;
        this.toTime.hours = 0;
        this.toTime.minutes = 0;
    }

    public showtime(): void {
        console.log('FromTime: ', this.fromTime);
        console.log('ToTime: ', this.toTime);
    }
    public setFromHours(value): void {
        console.log(value);
        this.fromTime.hours = value;
    }
    public setFromMinutes(value): void {
        console.log(value);
        this.fromTime.minutes = value;
    }
    public setToHours(value): void {
        console.log(value);
        this.toTime.hours = value;
    }
    public setToMinutes(value): void {
        console.log(value);
        this.toTime.minutes = value;
    }
}
class Time {
    public hours: number;
    public minutes: number;
}

import { Routes, Route } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { SpinnersComponent } from '@app/spinners/spinners.component';
import { INamedRoute } from '@src/shared/models/named-route';
import { TimePickerComponent } from '@app/time-picker/time-picker.component';


export const NAMED_ROUTES: INamedRoute[] = [
    { name: 'Home', route: { path: '', component: AppComponent }},
    { name: 'Spinners', route: { path: 'spinners', component: SpinnersComponent }},
    { name: 'Time Picker', route: { path: 'time-picker', component: TimePickerComponent }}
    // homeComponent: Route = { path: '', component: AppComponent, name: '' };
    // spinnersComponent: Route = { path: 'spinners', component: SpinnersComponent };
];

export const ROUTES: Routes = [
    {
        path: '',
        component: AppComponent
    },
    {
        path: 'spinners',
        component: SpinnersComponent
    },
    {
        path: 'time-picker',
        component: TimePickerComponent
    },
];

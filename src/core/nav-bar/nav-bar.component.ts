import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { ROUTES, NAMED_ROUTES } from '@core/routes';
import { INamedRoute } from '@src/shared/models/named-route';

@Component({
    selector: 'fancy-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
    private navRoutes: INamedRoute[];

    constructor() {
        this.navRoutes = NAMED_ROUTES;
    }

    ngOnInit() {
        console.log(this.navRoutes);
        // this.navRoutes.forEach((r1, index) => {
        //     this.navRoutes.forEach(r2 => {
        //         console.log(r1, r2);
        //         if (r1.route.path.includes(r2.path) && r2.path !== '' && r1.path !== r2.path) {
        //             this.navRoutes.splice(index, 1);
        //         }
        //     });
        // });
    }

}

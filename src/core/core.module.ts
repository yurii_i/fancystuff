import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from '@core/core-routing.module';
import { AppModule } from '@app/app.module';
import { CoreComponent } from '@core/core.component';
import { SharedModule } from '@src/shared/shared.module';
import { NavBarComponent } from '@core/nav-bar/nav-bar.component';
import { AppComponent } from '@app/app.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        NavBarComponent,
        CoreComponent,
    ],
    imports: [
        CommonModule,
        AppModule,
        SharedModule,
        RouterModule
    ],
    bootstrap: [CoreComponent]

})
export class CoreModule { }

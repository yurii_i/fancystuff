import { Route } from '@angular/router';

export class NamedRoute implements INamedRoute {
    route: Route;
    name: string;
}

export interface INamedRoute {
    route: Route;
    name: string;
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteFromComponentPipe } from './pipes/route-from-component/route-from-component.pipe';

@NgModule({
  declarations: [
      RouteFromComponentPipe
    ],
  exports: [
      RouteFromComponentPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
